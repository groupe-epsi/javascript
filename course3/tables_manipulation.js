// Exercise 1

let samples = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

for (let i = 0; i < samples.length; i++) {
  console.log(samples[i]);
}

for (const number of samples) {
  console.log(number);
}

samples.forEach(number => console.log(number));

console.log();
// Exercise 2

samples = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

let square = [];
samples.forEach(number => square.push(number ** 2));
console.debug('Square: ', square);

square = samples.map(number => number ** 2);
console.debug('Square without loop: ', square);

console.log();
// Exercise 3

samples = [1, 3, 2, 4, 5, 7, 6, 8, 10, 9];

let pairs = [];
samples.forEach(number => {
  if (number % 2 === 0) {
    pairs.push(number);
  }
});
console.debug('Pairs: ', pairs);

console.log();
// Exercise 4

samples = [1, 3, 2, 4, 5, 7, 6, 8, 10, 9];

console.log(samples);
console.log('Contains the value 10: ', samples.includes(10));
console.log('Contains odd number: ', samples.some(number => number % 2 === 1));

console.log();
// Exercise 5

samples = [11, 14, 29, 4, 18, 12, 1, 2, 6, 3];

let sum = 0;
samples.forEach(number => sum += number);
console.log('Sum: ', sum);

console.log('Sum without loop: ', samples.reduce((sum, number) => sum + number));

console.log();
// Exercise 6

const people = [
  { lastName: 'Dupont', firstName: 'Maxime', age: 14 },
  { lastName: 'Dubois', firstName: 'Aurélien', age: 16 },
  { lastName: 'Duriez', firstName: 'Eric', age: 19 },
  { lastName: 'Dutrain', firstName: 'Jean', age: 21 },
  { lastName: 'Dutronc', firstName: 'Grégoire', age: 18 }
];

console.log("People aged 18 or more: ", people.filter(person => person.age >= 18));
console.log("Sum of age: ", people.reduce((sum, person) => sum + person.age, 0) / people.length);
console.log("Sorted people: ", people.sort((a, b) => b.age - a.age));
