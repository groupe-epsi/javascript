// API root URL
const apiURL = 'https://jsonplaceholder.typicode.com/todos';

// HTML elements
const loadButtonElement = document.querySelector('#load-button');

const tableElement = document.querySelector('#table');
const tableContainerElement = document.querySelector('#table-container');
const todoCountElement = document.querySelector('#todo-count');

const addButtonElement = document.querySelector('#add-button');
const titleInputElement = document.querySelector('#title-input');

const headerCount = 1;
const footerCount = 0;

// Clone and store a row sample
let rowSample = document.querySelector('#row-sample').cloneNode(true);
document.querySelector('#row-sample').remove();

hideElement(tableContainerElement);
hideElement(todoCountElement);

// Listeners
loadButtonElement.addEventListener('click', () => displayAllTasks());
addButtonElement.addEventListener('click', event => createTask(event));

/**
 * Hide an html element
 * @param element
 */
function hideElement(element) {
    element.classList.add('hidden');
}

/**
 * Display an html element
 * @param element
 */
function displayElement(element) {
    element.classList.remove('hidden');
}

/**
 * Get all tasks from jsonplaceholder API and insert them into the table
 */
async function displayAllTasks() {
    const request = await fetch(apiURL);
    const todos = await request.json();

    resetTable(headerCount, footerCount);
    todos.forEach(addRow);
    displayElement(tableContainerElement);

    displayTaskCount(tableElement, headerCount, footerCount);
    displayElement(todoCountElement);
}

/**
 * Send task to jsonplaceholder API and insert it into the table
 * @param event
 */
async function createTask(event) {
    try {
        const task = {
            title: titleInputElement.value,
            complete: false
        };
        const request = await fetch(apiURL, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(task)
        });
        const result = await request.json();
        addRow(result);
        displayTaskCount(tableElement, headerCount, footerCount);
    } catch (e) {
        console.error(e);
    }
    event.preventDefault();
}

/**
 * Display the dropdown
 * @param taskID ID of the task's dropdown to display
 */
function displayEditDropdown(taskID) {
    const taskRow = document.querySelector('#task-' + taskID);
    const dropdown = taskRow.querySelector('#dropdown');
    const completeTask = dropdown.querySelector('#complete');
    const deleteTask = dropdown.querySelector('#delete');

    displayElement(dropdown);

    deleteTask.addEventListener('click', e => {
        this.deleteTask(taskID);
        hideElement(dropdown);
    });

    completeTask.addEventListener('click', e => {
        this.completeTask(taskID);
        hideElement(dropdown);
    });

    dropdown.addEventListener('blur', () => {
        hideElement(dropdown);
    });

    // hide dropdown when clicking outside
    document.body.addEventListener('click', e => {
        if (e.target !== dropdown) {
            hideElement(dropdown);
        }
    });
}

/**
 * Remove a task from the table
 * @param taskID The ID of the task to remove
 */
async function deleteTask(taskID) {
    await fetch(apiURL + '/' + taskID, {
        method: 'DELETE',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    });
    const taskRow = document.querySelector('#task-' + taskID);
    taskRow.remove();
    displayTaskCount(tableElement, headerCount, footerCount);
}

/**
 * Remove a task from the table
 * @param taskID The ID of the task to remove
 */
async function completeTask(taskID) {
    await fetch(apiURL + '/' + taskID, {
        method: 'PATCH',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify({ completed: true })
    });

    const taskRow = document.querySelector('#task-' + taskID);
    const statusNode = taskRow.querySelector('#status');

    statusNode.classList.add('bg-green-100', 'text-green-800');
    statusNode.textContent = 'Oui';
}

/**
 * Remove all the tbody rows
 * @param headerCount The number of thead rows
 * @param footerCount The number of tfoot rows
 */
function resetTable(headerCount, footerCount) {
    for (let i = tableElement.rows.length - 1; i > footerCount; i--) {
        tableElement.deleteRow(i);
    }
}

/**
 * Insert a task into the table
 * @param todo The task to add
 */
function addRow(todo) {
    const row = rowSample.cloneNode(true);
    row.id = 'task-' + todo.id;

    const idNode = row.querySelector('#id');
    const descriptionNode = row.querySelector('#description');
    const statusNode = row.querySelector('#status');
    let dropdownButton = row.querySelector('#dropdown-button');
    dropdownButton.addEventListener('click', e => {
        // stop propagation otherwise the dropdown will capture the event
        e.stopPropagation();
        displayEditDropdown(todo.id);
    });

    idNode.textContent = todo.id;
    descriptionNode.textContent = todo.title;
    if (todo.completed) {
        statusNode.classList.add('bg-green-100', 'text-green-800');
        statusNode.textContent = 'Oui';
    } else {
        statusNode.classList.add('bg-red-100', 'text-red-800');
        statusNode.textContent = 'Non';
    }

    tableElement.append(row);
    displayTaskCount(tableElement, headerCount, footerCount);
}

/**
 * Display the number of tasks
 * @param table The table containing tasks
 * @param headerCount The number of header rows
 * @param footerCount The number of foot rows
 */
function displayTaskCount(table, headerCount, footerCount) {
    const taskCount = table.rows.length - (headerCount + footerCount);
    todoCountElement.innerHTML = `${taskCount} tâches.`;
    displayElement(todoCountElement);
}
