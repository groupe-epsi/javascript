Objectifs du TP: 

- Comprendre le fonctionnement du protocole HTTP 
- Tester des requêtes à l’aide de Postman 
- Mieux comprendre l’API Promise de Javascript et son utilisation 
- Appréhender la syntaxe async/await en langage Javascript 
- Utiliser la méthode fetch() pour récupérer des informations depuis un serveur 
